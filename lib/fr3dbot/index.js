const { Client } = require("discord.js");
const emojiRegex = require('emoji-regex');
const Storage = require("./Storage");
const QOTDModule = require("./modules/QOTDModule");
const PubSubModule = require("./modules/PubSubModule");
const NotifyOfMentionsModule = require("./modules/NotifyOfMentionsModule");
const ReactionRolesModule = require("./modules/ReactionRolesModule");
const ReactionHandler = require("./handlers/ReactionHandler");
const CommandHandler = require("./handlers/CommandHandler");
const EffectHandler = require("./handlers/EffectHandler");
const UndoneCommands = require("./handlers/UndoneCommands");
const modules = global.config[process.env.NODE_ENV=="production"? "modules" : "devModules"];
const { storagePath, cachePath } = global.config;

class Fr3dbot {
	constructor() {
		// Init base
		this.client = new Client({
			allowedMentions: { users: []},
			presence: {
				activity: {
					name: "queen's requests",
					type: "LISTENING",
					url: "https://twitch.tv/thedogepuppy_",
				},
			},
			// presence: {
			// 	activity: {
			// 		name: "Fr3ddi",
			// 		type: "STREAMING",
			// 		url: "https://twitch.tv/thedogepuppy_",
			// 	},
			// },
		});
		this.storage = new Storage(storagePath);
		this.getCache = name => new Storage(cachePath+name+".json");

		// Init modules
		if (modules.qotd) this.qotd = new QOTDModule(this);
		if (modules.pubsub) this.pubsub = new PubSubModule(this);
		if (modules.nom) this.nom = new NotifyOfMentionsModule(this);
		if (modules.reactroles) this.reactroles = new ReactionRolesModule(this);

		// Init handlers
		this.undoneCommands = new UndoneCommands();
		this.commandHandler = new CommandHandler(this);
		this.effectHandler = new EffectHandler();
		this.reactionHandler = new ReactionHandler();

		// Bind event handlers
		this.client.once("ready", () => {
			this.loadData();
			console.log("Logged in as %s", this.client.user.tag);
		});

		if (process.env.NODE_ENV != "production") this.client.on('debug', console.log);
		this.client.on("warn", console.warn);
		this.client.on("error", console.error);

		this.client.on("message", async message => {
			if (message.channel.type != "text") return; // Ignore non-text and non-guild channels
			if (this.client.user.id == message.author.id) return; // Ignore own messages

			if (await this.effectHandler.handle(message)) return;

			if (!(await this.commandHandler.handle(message))) {
				this.undoneCommands.checkAddMessage(message.id);
			}
		});

		this.client.on("messageUpdate", async (message, newMessage) => {
			if (message.channel.type != "text") return; // Ignore non-text and non-guild channels
			if (this.client.user.id == message.author.id) return; // Ignore own messages

			if (await this.effectHandler.handle(newMessage)) return;

			if (this.undoneCommands.checkAddMessage(message.id, false)) {
				if (await this.commandHandler.handle(newMessage)) this.undoneCommands.removeMessage(message.id);
			}
		});

		this.client.on("raw", ({t: type, d: data}) => {
			if (!type) return;
			const event = {
				partial: true,
				message: { id: data.message_id },
				user: { id: data.user_id },
				emoji: data.emoji,
			};

			switch (type) {
				case "MESSAGE_REACTION_ADD":
					this.reactionHandler.handleAdd(event, event.user);
					break;
				case "MESSAGE_REACTION_REMOVE":
					this.reactionHandler.handleRemove(event, event.user);
					break;
			}
		});

		// this.client.on("messageReactionAdd", (reaction, user) =>
		// 	this.reactionHandler.handleAdd(reaction, user)
		// );
		//
		// this.client.on("messageReactionRemove", (reaction, user) =>
		// 	this.reactionHandler.handleRemove(reaction, user)
		// );
	}

	loadData() {
		if (modules.pubsub) this.pubsub.load();
		if (modules.nom) this.nom.load();

		// Load data of modules
    for (const [guildID, guildData] of Object.entries(this.storage.data.guilds)) {
			if (modules.reactroles && guildData.reactroles) {
        for (const item of guildData.reactroles) {
          this.reactroles.load(guildID, item);
        }
      }

			if (modules.qotd && guildData.qotd) {
        this.qotd.load(guildID, guildData.qotd);
      }
    }
	}

	start() {
		return this.client.login(process.env.DISCORD_TOKEN).then(() => this.client);
	}


	escapeMarkdown(text) {
		const unescaped = text.replace(/\\(\*|_|`|~|\\)/g, '$1'); // unescape any "backslashed" character
		const escaped = unescaped.replace(/(\*|_|`|~|\\)/g, '\\$1'); // escape *, _, `, ~, \
		return escaped;
	}

	resolveEmoji(str, skipResolve=false) {
		if (!str) return null;
		const customEmojiID = (str.match(/<:.+?:(\d+)>/) || [])[1];
		if (customEmojiID) {
			if (skipResolve) return customEmojiID;
			return this.client.emojis.resolve(customEmojiID);
		}
		return (str.match(emojiRegex) || [])[0];
	}

	resolveRole(str, guild, skipResolve=false) {
		if (!str) return null;
		const roleID = (str.match(/<@&(\d+)>/) || [0])[1];
		if (roleID) {
			if (skipResolve) return roleID;
			return guild.roles.resolve(roleID);
		}
	}

	resolveMember(str, guild, skipResolve=false) {
		if (!str) return null;
		const userID = (str.match(/<@!?(\d+)>/) || [0])[1];
		if (userID) {
			if (skipResolve) return userID;
			return guild.members.resolve(userID);
		}
	}

	resolveMention(str, guild, skipResolve=false) {
		if (!str) return null;
		return this.resolveRole(str, guild, skipResolve) || this.resolveMember(str, guild, skipResolve);
	}
}

module.exports = new Fr3dbot();
