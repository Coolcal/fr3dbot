const { MessageEmbed } = require("discord.js");
const schedule = require('node-schedule');

const notRepeat = .5; // %

module.exports = class QOTDModule {
  constructor(bot) {
    this.bot = bot;
    this.storage = bot.storage;
    this.cache = bot.getCache("qotd");

    this.jobs = {};
  }

  load(guildID, item) {
    if (item.schedule) this.schedule(guildID, item.schedule);
  }

  trigger(guildID) {
    if (guildID in this.jobs) this.jobs[guildID].cancelNext();

    return this._trigger(guildID);
  }

  _trigger(guildID) {
    const data = this.storage.getGuild(guildID, false);
    if (!data || !data.qotd || !data.qotd.questions.length) return false;
    const dontpost = this.cache.getGuild(guildID, []);
    const { questions } = data.qotd;

    // Drop questions posted long ago from "do not post" buffer
    while (dontpost.length / questions.length >= notRepeat) dontpost.shift();

    let question;
    do {
      question = questions[Math.floor(questions.length * Math.random())];
    } while (dontpost.includes(question[0]));

    dontpost.push(question[0]);
    this.cache.save();
    return this.post(guildID, ...question);
  }

  post(guildID, question, answers) {
    const data = this.storage.getGuild(guildID, false);
    if (!data || !data.qotd) return false;
    if (!answers || !answers.length) {
      answers = [
        ["✅", "Yes"],
        ["❌", "No"],
      ];
    }

    const { channel: channelID, ping } = data.qotd;

    return this.bot.client.channels.fetch(channelID).then(async channel => {
      const embed = new MessageEmbed()
        .setColor(0x0011ff)
        .setTitle("QOTD: "+question)
        .setDescription(answers.map(([emoji, answer]) =>
          `${this.bot.resolveEmoji(`<:unnamed:${emoji}>`)} - ${answer}`
        ).join("\n"));
      let message, promise = channel.send(ping ? `<@&${ping}>` : "", {
        embed, allowedMentions: ping ? { roles: [ping] } : {},
      }).then(m => message = m);

      for (const [emoji] of answers) {
        const _emoji = this.bot.resolveEmoji(`<:unnamed:${emoji}>`);
        promise = promise.then(() =>
          message.react(_emoji.id || emoji)
        );
      }
      return promise;
    });
  }

  setup(guildID, channelID, ping) {
    const data = this.storage.getGuild(guildID, false);
    if (data && data.qotd) return false;
    Object.assign(data.qotd = data.qotd || {}, {
      channel: channelID,
      ping,
      schedule: false,
      questions: [],
    });
    this.storage.save();
    return true;
  }

  clear(guildID) {
    const data = this.storage.getGuild(guildID, false);
    if (!data || !data.qotd) return false;
    this.unschedule(guildID);

    delete data.qotd;
    this.storage.save();
    return true;
  }

  schedule(guildID, hour) {
    const data = this.storage.getGuild(guildID, false);
    if (!data || !data.qotd) return false;

    data.qotd.schedule = hour;
    this.storage.save();

    return this._schedule(guildID, hour);
  }

  _schedule(guildID, hour) {
    if (guildID in this.jobs) return false;
    this.jobs[guildID] = schedule.scheduleJob({hour: +hour, minute: 0}, () => {
      this._trigger(guildID);
    });
    return true;
  }

  unschedule(guildID) {
    if (!(guildID in this.jobs)) return false;
    this.jobs[guildID].cancel();
    delete this.jobs[guildID];

    const data = this.storage.getGuild(guildID, false);
    if (!data || !data.qotd) return false;
    data.qotd.schedule = false;
    this.storage.save();
    return true;
  }

  add(guildID, question, answers) {
    const data = this.storage.getGuild(guildID, false);
    if (!data || !data.qotd) return false;

    if (!answers || !answers.length) {
      answers = null;
    }

    data.qotd.questions.push([
      question,
      answers,
    ]);
    this.storage.save();
    return true;
  }

  remove(guildID, index) {
    const data = this.storage.getGuild(guildID, false);
    if (!data) return false;
    data.qotd.questions.splice(+index, 1);
    this.storage.save();
    return true;
  }
};
