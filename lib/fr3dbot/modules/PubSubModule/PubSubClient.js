const got = require("got");
const webhookListener = require("../../../webhookListener");

module.exports = class PubSubClient {
  constructor(name, hubURL, topicURL, onNotification=()=>{}, searchparams=false) {
    this.name = name;
    this.hubURL = hubURL;
    this.topicURL = topicURL;
    this.searchparams = searchparams;
    this.subscriptions = {};

    webhookListener.all("/"+name+"/:id", async (req, res) => {
      if (!(req.params.id in this.subscriptions)) return res.status(404).send("Unwanted");
      if (req.query["hub.challenge"]) return res.send(req.query["hub.challenge"]);

      res.send("OK");
      onNotification(req.params.id, req.body);
    });
  }

  async _makeSubscription(id, headers) {
    return got.post(this.hubURL, {
      ...(headers ? {headers} : {}),
      [this.searchparams ? "searchParams" : "json"]: {
        "hub.callback": encodeURI(process.env.WEBHOOK_URL+"/"+this.name+"/"+id),
        "hub.mode": "subscribe",
        "hub.lease_seconds": 864000,
        "hub.topic": encodeURI(this.topicURL.replace("{}", id)),
      },
    }).then(() => {
      // Refresh timeout
      this.subscriptions[id] = setTimeout(() => {
        this._makeSubscription(id);
      }, 864000 * 1000);
      return true;
    });
  }

  async subscribe(id, headers) {
    if (id in this.subscriptions) return;

    return this._makeSubscription(id, headers);
  }

  async unsubscribe(id, headers) {
    if (!(id in this.subscriptions)) return false;

    clearTimeout(this.subscriptions[id]);

    return got.post(this.hubURL, {
      ...(headers ? {headers} : {}),
      [this.searchparams ? "searchParams" : "json"]: {
        "hub.callback": encodeURI(process.env.WEBHOOK_URL+"/"+this.name+"/"+id),
        "hub.mode": "unsubscribe",
        "hub.lease_seconds": 0,
        "hub.topic": encodeURI(this.topicURL.replace("{}", id)),
      }
    });
  }
};
