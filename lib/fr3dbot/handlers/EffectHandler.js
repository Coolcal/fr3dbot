module.exports = class EffectHandler {
  constructor() {
    this.effects = [];
  }

  register(effect, priority=false) {
    if (this.effects.indexOf(effect) != -1) return false;

    if (priority) this.effects.unshift(effect);
    else this.effects.push(effect);

    return true;
  }

  unregister(effect) {
    const index = this.effects.indexOf(effect);
    if (index == -1) return false;
    this.effects.splice(index, 1);
    return true;
  }

  async handle(message) {
    for (const effect of this.effects) {
      if (await effect(message)) return true;
    }
    return false;
  }
};
