module.exports = class ReactionHandler {
  constructor() {
    this.addListeners = {};
    this.removeListeners = {};
  }

  listenAdd(messageID, callback) {
    (this.addListeners[messageID] = this.addListeners[messageID] || []).push(callback);
  }

  listenRemove(messageID, callback) {
    (this.removeListeners[messageID] = this.removeListeners[messageID] || []).push(callback);
  }

  handleAdd(reaction, user) {
    const handlers = this.addListeners[reaction.message.id];
    if (handlers) handlers.forEach(handler => handler(reaction, user));
  }

  handleRemove(reaction, user) {
    const handlers = this.removeListeners[reaction.message.id];
    if (handlers) handlers.forEach(handler => handler(reaction, user));
  }
}
