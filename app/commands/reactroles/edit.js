module.exports = async function editReactroles(args, {channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  if (!args[1]) throw "syntax";

  const item = this.bot.reactroles.getItem(guild.id, args[1]);
  if (!item) {
    channel.send("The ID doesn't exist");
    return false;
  }

  const [field, ...values] = args.slice(2);
  const value = values.join(" ");
  const editable = this.bot.reactroles.constructor.editableFields;

  // Render fields
  if (!field || !(field in editable)) {
    const fields = [];

    for (const field of Object.keys(editable)) {
      fields.push(`${field}: ${item[field] || null}`);
    }

    channel.send(
      (field ? `Unknown field '${field}'\n` : "")+
      `Fields for reactroles with ID ${item.id}:`+
      "```yaml\n"+ fields.join("\n") +"\n```"
    );
    return !field;
  }

  if (!testType(value, editable[field])) {
    channel.send(`The value you supplied cannot be converted to ${editable[field].name}`);
    return false;
  }

  let typedValue;
  switch (editable[field].name) {
    // Patched types
    case "Color":
      typedValue = (new (editable[field])(value)).hex();
      break;
    case "Boolean":
      typedValue = !["no", "false", "0", "disable", "disabled"].includes(value);
      break;
    // Default retyping
    default:
      typedValue = new (editable[field])(value);
  }
  item[field] = typedValue;

  if (await this.bot.reactroles.recreateEmbed(guild.id, args[1])) {
    channel.send("Success.");
  } else {
    channel.send("Failed to recreate reactroles");
  }
  return true;
};

function testType(value, Type) {
  try {
    return !!(new Type(value));
  } catch(_) {
    return false;
  }
}

module.exports.args = "<id> [field [value]]";
module.exports.description = "Edit a reactroles message. If you only specify the id, a list of editable fields will be sent.";
module.exports.admin = true;
