const table = require("text-table");

module.exports = async function listReactroles(args, {channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  const items = this.bot.reactroles.cache[guild.id];

  channel.send(items && items.length ?
    "Reactroles on this server:\n"+
    "```\n"+
    table([
      ["id", "title"],
      ...items.map(item => ([item.id, item.title || "Untitled"])),
    ])+
    "```"
    :
    "There are no reactroles created for this server."
  );

  return true;
};

module.exports.args = "";
module.exports.description = "Lists all reactroles messages on this server";
module.exports.admin = true;
