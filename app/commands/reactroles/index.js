module.exports = {
  description: "Allow your members to give themselves roles by reacting to a message.",
  admin: true,
};
