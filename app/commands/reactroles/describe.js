module.exports = async function removeReactroles(args, {channel, guild}) {
  if (!this.bot.reactroles) throw "disabled";
  const role = this.bot.resolveRole(args[2], guild);
  if (args.length < 3 || !role) throw "syntax";

  if (await this.bot.reactroles.describe(guild.id, args[1], role, args.slice(3).join(" ").trim())) {
    channel.send("Success.");
    return true;
  } else {
    channel.send("The ID doesn't exist.");
    return false;
  }
};

module.exports.args = "<id> <@role> [description]";
module.exports.description = "Add or change description of the specified role.";
module.exports.admin = true;
