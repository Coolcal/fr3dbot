const table = require("text-table");

module.exports = async function qotdList(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  const data = this.storage.getGuild(guild.id);
  if (data && data.qotd) {
    channel.send(
      "QOTD on this server happens in "+guild.channels.resolve(data.qotd.channel).toString()+
      (data.qotd.ping ? " and pings the role @"+(await guild.roles.fetch(data.qotd.ping)).name : "")+
      "```\n"+
      table([
        ["", "Question", "Answers"],
        ...data.qotd.questions.map(([question, answers], i) =>
          [
            i,
            question,
            answers ? answers.map(([emoji, answer]) =>
              this.bot.resolveEmoji(`<:unnamed:${emoji}>`)+" - "+answer
            ).join("; ") : "Yes/No",
          ]
        ),
      ])+
      "```\n"
    );
  } else {
    channel.send("This server doesn't have QOTD setup.");
  }
  return true;
};

module.exports.description = "List this server's QOTD settings";
module.exports.admin = true;
