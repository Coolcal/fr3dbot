module.exports = async function nomTrack(args, {channel, mentions, guild}) {
  if (!this.bot.nom) throw "disabled";

  const trackedRole = mentions.roles.firstKey() || args[2];
  const ping = mentions.roles.keyArray()[1] || mentions.users.firstKey();

  if (args.length < 3 || !trackedRole || !ping) throw "syntax";
  if (!await guild.roles.fetch(trackedRole)) {
    channel.send("Tracked role ID is invalid");
    return false;
  }

  if (this.bot.nom.track(args[1], ping, trackedRole, args.slice(3).join(" "))) {
    channel.send("Success");
    return true;
  } else {
    channel.send("Failed to create tracking");
    return false;
  }

}

module.exports.args = "<role ID/user ID to watch for> <@role who is being watched> [message including user @ping]";
module.exports.description = "Notify someone of mentions of someone else";
module.exports.admin = true;
