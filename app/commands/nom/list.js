const table = require("text-table");

module.exports = async function nomList(args, {channel, guild}) {
  if (!this.bot.nom) throw "disabled";

  const items = [];
  for (const [mention, {trackedRole, message}] of Object.entries(this.bot.nom.trackedMentions)) {
    const mentionResolved = await guild.members.fetch(mention) || await guild.roles.fetch(mention);
    const trackedRoleResolved = await guild.roles.fetch(trackedRole);
    if (!mentionResolved || !trackedRoleResolved) continue;

    items.push([
      mention + " (@"+(mentionResolved.name || mentionResolved.user.username)+")",
      "@"+trackedRoleResolved.name,
      message,
    ]);
  }

  channel.send(items && items.length ?
    "Mention trackings on this server:\n"+
    "```\n"+
    table([
      ["Watched mention", "Role being watched", "Message"],
      ...items,
    ])+
    "```"
    :
    "There are no mention trackings created for this server."
  );

  return true;
};

module.exports.args = "";
module.exports.description = "List currently tracked mentions";
module.exports.admin = true;
