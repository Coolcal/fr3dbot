const editTimeThreshold = 1.8e+6; // 30 minutes
const alreadyPostedLength = 32;

module.exports = class YoutubePubSub {
  constructor(bot, PubSubClient) {
    this.subscriptions = {};
    this.alreadyPosted = []; // ring buffer

    this.pubsub = new PubSubClient(
      "youtubeNotify",
      "https://pubsubhubbub.appspot.com/subscribe",
      "https://www.youtube.com/xml/feeds/videos.xml?channel_id={}",
      (id, {feed: {entry}}) => {
        if (!entry) return; // Video deleted

        if (this.alreadyPosted.includes(entry.id)) return; // Ignore already posted
        if (new Date(entry.updated) - new Date(entry.published) > editTimeThreshold) return; // Ignore edits

        this.alreadyPosted.push(entry.id);
        if (this.alreadyPosted.length > alreadyPostedLength) this.alreadyPosted.shift();

        const subscription = this.subscriptions[id];

        for (const [channelID, {mention}] of Object.entries(subscription.channels)) {
          const message = bot.escapeMarkdown(entry.author.name)+" has just published a video!"+
            (mention ? " <@&"+mention+">" : "" )+
            " "+entry.link.$.href;

          bot.client.channels.fetch(channelID).then(channel =>
            channel.send(message, { allowedMentions: { roles: [mention] } })
          );
        }
      },
      true
    );
  }

  async subscribe(id, channelID, mention) {
    if (!(id in this.subscriptions)) {
      this.subscriptions[id] = {
        promise: this.pubsub.subscribe(id),
        channels: {},
      };
    } else {
      // Prevent duplicates
      if (channelID in this.subscriptions[id].channels) {
        return false;
      }
    }

    this.subscriptions[id].channels[channelID] = {
      mention
    };

    return this.subscriptions[id].promise
      .then(() => true)
      .catch(({response}) => console.error(response.body) && false);
  }

  async unsubscribe(id, channelID) {
    if (!(id in this.subscriptions)) return false;

    const subscription = this.subscriptions[id];
    delete subscription.channels[channelID];

    // If no subscribers left, cancel the subscription enitrely
    if (!Object.keys(subscription.channels).length) return this.pubsub.unsubscribe(id);
    return true;
  }
};
