const got = require("got");
const { MessageEmbed } = require("discord.js");

const checkInterval = 15*60*1000; // 15 minutes

module.exports = class InstagramWatcher { // a PubSub but not really
  constructor(bot) {
    this.bot = bot;
    this.subscriptions = {};
    this.interval = setInterval(this.update.bind(this), checkInterval);
  }

  update() {
    const promises = [];

    for (const [name, subscription] of Object.entries(this.subscriptions)) {
      promises.push(this.getData(name).then(data => {
        if (!data) return; // not found, what to do?

        const user = data.graphql.user;
        const lastPost = user.edge_owner_to_timeline_media.edges[0].node;

        if (lastPost.taken_at_timestamp*1000 <= subscription.lastTime) return false; // Nothing new
        subscription.lastTime = lastPost.taken_at_timestamp*1000;

        for (const [channelID, {mention}] of Object.entries(subscription.channels)) {
          const message = this.bot.escapeMarkdown(user.full_name)+" has just posted on Instagram!"+
            (mention ? " <@&"+mention+">" : "" );
          const embed = new MessageEmbed()
            .setColor(0xf46f30)
            .setTitle("New post on Instagram")
            .setDescription(lastPost.edge_media_to_caption.edges[0].node.text)
            .setImage(lastPost.thumbnail_src || lastPost.display_url)
            .setTimestamp(lastPost.taken_at_timestamp*1000)
            .setURL("https://instagram.com/p/"+lastPost.shortcode)
            .setAuthor(`${user.full_name} (@${user.username})`, user.profile_pic_url, "https://instagram.com/"+user.username);

          this.bot.client.channels.fetch(channelID).then(channel =>
            channel.send(message, { embed, allowedMentions: { roles: [mention] } })
          );
        }
      }));
    }

    return Promise.all(promises);
  }

  async getData(name) {
    return got("https://www.instagram.com/{}/?__a=1".replace("{}", name), {
      headers: {
        "Host":	"www.instagram.com",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "cs,en-US;q=0.7,en;q=0.3",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "none",
        "Sec-Fetch-User": "?1",
        "Sec-GPC": "1",
        "Upgrade-Insecure-Requests": "1",
        "Cookie": process.env.IG_COOKIE,
      },
      responseType: "json",
    }).then(({body})=>body).catch(({response:{body}})=>console.log(body) && null);
  }

  async subscribe(name, channelID, mention) {
    if (!(name in this.subscriptions)) {
      if (!(await this.getData(name))) return false; // Doesn't exist

      this.subscriptions[name] = {
        channels: {},
        lastTime: Date.now(),
      };
    } else {
      // Prevent duplicates
      if (channelID in this.subscriptions[name].channels) {
        return false;
      }
    }

    this.subscriptions[name].channels[channelID] = {
      mention
    };

    return true;
  }

  async unsubscribe(name, channelID) {
    if (!(name in this.subscriptions)) return false;

    const subscription = this.subscriptions[name];
    delete subscription.channels[channelID];

    // If no subscribers left, cancel the subscription enitrely
    if (!Object.keys(subscription.channels).length) delete subscription[name];
    return true;
  }
};
